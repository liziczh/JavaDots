package com.lizi.test;

import com.lizi.company.pojo.Company;
import com.lizi.company.pojo.Emp;
import com.lizi.company.pojo.SubCompany;
import com.lizi.company.service.ICompanyService;
import com.lizi.company.service.IEmpService;
import com.lizi.company.service.ISubCompanyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class CompanyServiceTest {
    @Autowired
    private ICompanyService companyService;

    @Autowired
    private ISubCompanyService subCompanyService;

    @Autowired
    private IEmpService empService;

    @Test
    public void getCompanyListTest(){
        List<Company> companyList = companyService.getCompanyList();
        System.out.println(companyList);
    }

    @Test
    public void getSubCompanyListTest(){
        List<SubCompany> subCompanyList = subCompanyService.getSubCompanyListByComId(1);
        System.out.println(subCompanyList);
    }

    @Test
    public void getEmpTestListTest(){
        List<Emp> empList = empService.getEmpListBySubComId(1);
        System.out.println(empList);
    }
}
